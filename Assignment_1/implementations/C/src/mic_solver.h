#ifndef MIC_SOLVER_H_INCLUDED
#define MIC_SOLVER_H_INCLUDED

#include "main.h"

/* Constants */
#define N_SHIPS 4


/* Global variables */
int number_shots;

int remained_ships[N_SHIPS];

coordinate* isolated_arrays;


/* Data structures */
typedef enum
{
    SHOT_FREE     = -1,
    SHOT_BOX_SHIP = -2,
    EXCLUDED      = -3,
    SEA           = 1,
    SHIP          = 2
} LOCATION;

typedef enum
{
    RED,
    BLUE,
    GREEN
} ZONE;


/* Functions */
int solve (void);

int try_to_shot (coordinate point);

int shot (coordinate point);

int shot_ship (coordinate point_hit, int is_vertical);

int shot_coloured_zone (int color, coordinate* points);

coordinate* get_horizontal_neighbors (coordinate point);

coordinate* get_vertical_neighbors (coordinate point);

void eliminate_neighbors_ship (coordinate start, coordinate end, int blocks);

int n_remained_ships (void);

void remove_ship (int blocks);

int contains_ship (int blocks);

int count_occurences (int value);

int finish (void);

#endif
