#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

/* Constants */
#define N 10                                                                    /* Number of rows and columns of the matrix */
#define PATH_SOLVING_PERMUTATIONS "../../../resources/permutation_%d.txt"       /* For option "cs" */
#define PATH_CORRECT_CONFIGURATIONS "../../../resources/correct_conf_%d.txt"    /* For option "c" */


/* Global variables */
int board[N][N];            /* Working board */

int iboard[N][N];           /* Input board   */

int permutation_k;          /* Number of permutation */


/* Data structures */
typedef struct
{
    int r;      /* Row    */
    int c;      /* Column */
} coordinate;

typedef enum
{
    OPTION_C,
    OPTION_CS,
    OPTION_S,
    OPTION_A
} option_;


/* Functions */
void help (void);

void option_c_or_cs (option_ opt);

void option_s (char* file);

void option_a (void);

void error (char* message);

void ship_combinations (void);

void init_matrix (int m[N][N], int value);

void init_matrix_with_blocks (int m[N][N], int blocks, int horizontal);

void permute_matrix (int m_source[N][N], int m_destination[N][N], int blocks, int horizontal);

void get_correct_configurations (int m_1[][N][N], int l_1, int m_2[][N][N], int l_2,
                                 int m_3[][N][N], int l_3, int m_4[][N][N], int l_4, option_ opt);

int constraint_satisfied (int m[N][N], coordinate* coordinates, int blocks);

coordinate get_first_box_ship (int m[N][N]);

coordinate get_last_box_ship (int m[N][N]);

coordinate* get_first_and_last_box_ship (int m[N][N]);

void reset_row (int m[N][N], int row);

void reset_column (int m[N][N], int column);

void copy_values_matrixes (int m_source[N][N], int m_destination[N][N]);

void print_matrix (int m[N][N]);

void print_matrix_in_file (FILE* fp, int m_5[N][N], int m_4[N][N], int m_3[N][N], int m_2[N][N]);

void echo_grid_notation (FILE* fp, int m[N][N], int blocks);

int file_exists (char* file);

int is_number (char str[]);

void assert__ (int condition, char* message);

#endif
