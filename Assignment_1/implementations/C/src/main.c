#include "main.h"
#include "mic_solver.h"

int
main (int argc, char** argv)
{
    int k;

    if (argc == 1) {
        help ();

        exit (-1);
    }

    /* Command line parsing */
    for (k = 1; k < argc; k++) {
        /* Option "-cs". We assume precedence for this option (if the user select another option we handle only this) */
        if (!strcmp (argv[k], "-cs"))
            option_c_or_cs (OPTION_CS);
        /* Option "-c" */
        else if (!strcmp (argv[k], "-c"))
            option_c_or_cs (OPTION_C);
        /* Option "-s" */
        else if (!strcmp (argv[k], "-s")) {
            if (++k < argc) {
                if (strlen (argv[k]) > 0 && (file_exists (argv[k])))
                    option_s (argv[k]);
                else
                    error ("File does not exist");
            }
        }
        else if (!strcmp (argv[k], "-a"))
            option_a ();
        else {
            help ();

            break;
        }
    }

    return 0;
}

void
help (void)
{
    printf ("Usage: main [-h | -cs | -c | -s <file>]\n");
    printf ("-h         print this help;\n");
    printf ("-cs        generate all possible combinations of the ships and apply the algorithm;\n");
    printf ("-c         generate all possible combinations of the ships;\n");
    printf ("-s <file>  algorithm will be applied on the file passed as input;\n");
    printf ("-a         performed statistical analysis based on the results produced by option \"cs\".\n");
}

void
option_c_or_cs (option_ opt)
{
    int k = 0,
        j = 0,
        i = 0,
        x = 0;    /* Indices */

    /* Global variable */
    permutation_k = 1;

    /* How many configurations for ship with length of 2?
     *      - vertically: 9 for each row => 9 * 10 = 90;
     *      - horizontally: 10 for each 2 columns => 10 * 9 = 90 (9 is all the possible couples of columns [0,1],[1,2],[2,3],...,[8,9].
     *      ==> 90 + 90 = 180 possible positions of ship with length 2.
     */
    int m_2[180][N][N];

    /* How many configurations for ship with length of 3? If we follow the same method used for ship we length 2, we obtain:
     *      - vertically: 8 * 10 = 80;
     *      - horizontally: 10 * 8 = 80.
     *      ==> 80 + 80 = 160 possible positions of ship with length 3.
     */
    int m_3[160][N][N];

    /* How many configurations for ship with length of 4?
     *      - vertically: 7 * 10 = 70;
     *      - horizontally: 7 * 10 = 70.
     *      ==> 70 + 70 = 140 possible positions of ship with length 4.
     */
    int m_4[140][N][N];

    /* How many configurations for ship with length of 5?
     *      - vertically: 6 * 10 = 60;
     *      - horizontally: 10 * 6 = 60.
     *      ==> 60 + 60 = 120 possible positions of ship with length 5.
     */
    int m_5[120][N][N];


    /*
     * If we consider the permutations of the {2, 3, 4, 5} we obtain 4! = 24 differents "sets". Each set contains
     * N = 180 * 160 * 140 * 120 = 483.840.000, possible configurations. So, total is: T = N * 24 = 11.612.160.000.
     * Anyway, not all configurations are correct since we must respect the constraints imposed by the deliver.
     * So, surely we obtain a number of configurations N_C, with N_C <= T
     */

    /* Possible combinations for ships with length 2 */
    /* Initialize the first 2 ship of length 2 (vertically and horizontally oriented) */
    init_matrix_with_blocks (m_2[k++], 2, 1);   /* Horizontal */
    init_matrix_with_blocks (m_2[k++], 2, 0);   /* Vertical */

    while (k < 180) {
        /* Horizontal */
        permute_matrix (m_2[k - 2], m_2[k++], 2, 1);

        /* Vertical */
        permute_matrix (m_2[k - 2], m_2[k++], 2, 0);
    }

    /* Possible combinations for ships with length 3 */
    /* Initialize the first 2 ship of length 3 (vertically and horizontally oriented) */
    k = 0;
    init_matrix_with_blocks (m_3[k++], 3, 1);   /* Horizontal */
    init_matrix_with_blocks (m_3[k++], 3, 0);   /* Vertical */

    while (k < 160) {
        /* Horizontal */
        permute_matrix (m_3[k - 2], m_3[k++], 3, 1);

        /* Vertical */
        permute_matrix (m_3[k - 2], m_3[k++], 3, 0);
    }

    /* Possible combinations for ships with length 4 */
    /* Initialize the first 2 ship of length 4 (vertically and horizontally oriented) */
    k = 0;
    init_matrix_with_blocks (m_4[k++], 4, 1);   /* Horizontal */
    init_matrix_with_blocks (m_4[k++], 4, 0);   /* Vertical */

    while (k < 140) {
        /* Horizontal */
        permute_matrix (m_4[k - 2], m_4[k++], 4, 1);

        /* Vertical */
        permute_matrix (m_4[k - 2], m_4[k++], 4, 0);
    }

    /* Possible combinations for ships with length 5 */
    /* Initialize the first 2 ship of length 5 (vertically and horizontally oriented) */
    k = 0;
    init_matrix_with_blocks (m_5[k++], 5, 1);   /* Horizontal */
    init_matrix_with_blocks (m_5[k++], 5, 0);   /* Vertical */

    while (k < 120) {
        /* Horizontal */
        permute_matrix (m_5[k - 2], m_5[k++], 5, 1);

        /* Vertical */
        permute_matrix (m_5[k - 2], m_5[k++], 5, 0);
    }


    /* Permutations (too lazy to write 24 permutations) */
    for (k = 2; k <= 5; k++) {
        for (j = 2; j <= 5; j++) {
            if (k == j)
                continue;
            for (i = 2; i <= 5; i++) {
                if (k == i || j == i)
                    continue;
                for (x = 2; x <= 5; x++) {
                    if (k == x || j == x || i == x)
                        continue;

                    printf ("[+] Permutation %d:   [%d, %d, %d, %d]\n", permutation_k, k, j, i, x);

                    get_correct_configurations (
                        /* First element */
                        k == 2 ? m_2 : k == 3 ? m_3 : k == 4 ? m_4 : m_5,
                        k == 2 ? 180 : k == 3 ? 160 : k == 4 ? 140 : 120,
                        /* Second element */
                        j == 2 ? m_2 : j == 3 ? m_3 : j == 4 ? m_4 : m_5,
                        j == 2 ? 180 : j == 3 ? 160 : j == 4 ? 140 : 120,
                        /* Third element */
                        i == 2 ? m_2 : i == 3 ? m_3 : i == 4 ? m_4 : m_5,
                        i == 2 ? 180 : i == 3 ? 160 : i == 4 ? 140 : 120,
                        /* Fourth element */
                        x == 2 ? m_2 : x == 3 ? m_3 : x == 4 ? m_4 : m_5,
                        x == 2 ? 180 : x == 3 ? 160 : x == 4 ? 140 : 120,
                        opt
                    );

                    exit (-1);

                    /* Global variable */
                    permutation_k++;
                }
            }
        }
    }

    exit (0);
}

void
option_s (char* file)
{
    /* We already checked if the file exists */
    FILE* fp = fopen (file, "r");
    char line[80];
    int counter = 0, n_configurations = 0, len, k, n_line = 0, n_points = 0;
    int m[10][10];
    int char_;
    coordinate point;

    while (fgets (line, 80, fp) != NULL) {
        n_line++;
        len = strlen (line);

        /* Line could contain correct position of the ship */
        if (len > 0 && (line[0] != '\n' || line[len - 1] != '\n')) {
            if (counter == 0)
                init_matrix (m, SEA);

            /* Parsing the line */
            k = 0;
            n_points = 0;
            while (k < len && line[k] != '\n') {
                char_ = line[k];

                point.c = (char_ >= 'a' && char_ <= 'j') ? char_ - 'a' :
                          (char_ >= 'A' && char_ <= 'J') ? char_ - 'A' : -1;

                if (point.c == -1) {
                    printf ("option_s (), line %d of the file, %c is unknown character for columns.\n", n_line, char_);

                    exit (-1);
                }

                /* ASCII of '1' is 4. If coordinate is B1, 1 is the row but the initial index is 0 in the matrix */
                point.r = (k + 1 < len && (line[k + 1] >= '1' && line[k + 1] <= '9')) ? line[k + 1] - 49 : -1;

                if (point.r == -1) {
                    printf ("option_s (), line %d of the file, %c is unknown character for rows.\n", n_line, line[k + 1]);

                    exit (-1);
                }

                /* r could be 10 */
                point.r = (k + 2 < len && line[k + 2] == '0') ? 9 : point.r;

                /* 1 increment for the column (one character), 1 or 2 increment for the row: 1 if value of row is between 1 and 9, 2 else (row=10) */
                k += (point.r == 9) ? 3 : 2;

                /* Additional control to be sure */
                if (point.r < 0 || point.r > 10) {
                    printf ("option_s (), line %d of the file, %d is unknown number for rows.\n", n_line, point.r);

                    exit (-1);
                }

                m[point.r][point.c] = SHIP;

                n_points++;
            }

            counter++;

            if (counter == 1 && n_points != 5) {
                printf ("option_s (), line %d of the file, we encountered %d coordinates instead of 5.\n", n_line, n_points);

                exit (-1);
            }
            else if (counter == 2 && n_points != 4) {
                printf ("option_s (), line %d of the file, we encountered %d coordinates instead of 4.\n", n_line, n_points);

                exit (-1);
            }
            else if (counter == 3 && n_points != 3) {
                printf ("option_s (), line %d of the file, we encountered %d coordinates instead of 3.\n", n_line, n_points);

                exit (-1);
            }
            else if (counter == 4 && n_points != 2) {
                printf ("option_s (), line %d of the file, we encountered %d coordinates instead of 2.\n", n_line, n_points);

                exit (-1);
            }

            if (counter == 4) {
                n_configurations++;
                copy_values_matrixes (m, board);

                printf ("----------------------------------------------------------------------------\n");
                printf ("Configuration number %d. Number of shots: %d.\n", n_configurations, solve ());
                print_matrix (m);
                printf ("----------------------------------------------------------------------------\n\n");

                counter = 0;
            }
        }
    }

    fclose (fp);
}

void
option_a (void)
{
    int N_PERMUTATIONS = 24, min = 100, max = 0, local_cont, num, len, k;

    FILE* fp;

    char file_name[100], line[80];

    long int sum = 0;

    double mean, st_devation, sum_x_i_less_min = (double) 0, sum_x_i_less_min_power_two = (double) 0, app;

    for (k = 1; k <= N_PERMUTATIONS; k++) {
        sprintf (file_name, PATH_SOLVING_PERMUTATIONS, k);

        printf ("[+] Parsing file \"%s\".\n", file_name);

        fp = fopen (file_name, "r");

        if (fp == NULL)
            error ("[-] fp is null");

        local_cont = 0;
        while (fgets (line, 80, fp) != NULL) {
            len = strlen (line);
            if (len > 0 && line[0] != '\n') {
                local_cont++;

                num = atoi (line);

                sum += num;

                if (num > max)
                    max = num;
                else if (num < min)
                    min = num;
            }
        }

        assert__ (local_cont == 80194520, "Number of configurations is not 80194520.");

        local_cont = 0;

        fclose (fp);
    }

    /* We already checked that, in each iteration, number of configurations are 80194520 */
    mean = sum / (double) (80194520 * N_PERMUTATIONS);

    for (k = 1; k <= N_PERMUTATIONS; k++) {
        sprintf (file_name, PATH_SOLVING_PERMUTATIONS, k);

        printf ("[+] Parsing file \"%s\".\n", file_name);

        fp = fopen (file_name, "r");

        if (fp == NULL)
            error ("[-] fp is null");

        local_cont = 0;
        while (fgets (line, 80, fp) != NULL) {
            len = strlen (line);
            if (len > 0 && line[0] != '\n') {
                local_cont++;

                app = (double) atoi (line) - mean;

                sum_x_i_less_min += app;

                sum_x_i_less_min_power_two += pow (app, 2);
            }
        }

        assert__ (local_cont == 80194520, "Number of configurations is not 80194520.");

        local_cont = 0;

        fclose (fp);
    }

    st_devation = sqrt (sum_x_i_less_min_power_two / (80194520 * N_PERMUTATIONS));

    printf ("[+] Analysis start.\n\n");
    printf ("\t[+] Minimum:        %d.\n", min);
    printf ("\t[+] Maximum:        %d.\n", max);
    printf ("\t[+] Sum:            %ld.\n", sum);
    printf ("\t[+] Mean:           %f.\n", mean);
    printf ("\t[+] x_i - mean:     %f.\n", sum_x_i_less_min);
    printf ("\t[+] (x_i - mean)^2: %f.\n", sum_x_i_less_min_power_two);
    printf ("\t[+] st. devation:   %f.\n", st_devation);
    printf ("\n[+] Analysis end.\n\n");
}

void
error (char* message)
{
    printf ("Error: %s!\n", message);

    exit (-1);
}

void
init_matrix (int m[N][N], int value)
{
    int k, j;

    for (k = 0; k < N; k++)
        for (j = 0; j < N; j++)
            m[k][j] = value;
}

void
init_matrix_with_blocks (int m[N][N], int blocks, int horizontal)
{
    int k;

    init_matrix (m, SEA);

    /* Both for horizontal and vertical */
    m[0][0] = SHIP;

    m[(horizontal) ? 0 : 1 ][(horizontal) ? 1 : 0] = SHIP;

    for (k = 3; k <= blocks; k++)
        m[(horizontal) ? 0 : k - 1][(horizontal) ? k - 1 : 0] = SHIP;
}

void
permute_matrix (int m_source[N][N], int m_destination[N][N], int blocks, int horizontal)
{
    coordinate point;
    int k, new_c;

    copy_values_matrixes (m_source, m_destination);

    point = get_first_box_ship (m_destination);
    /* If r = -1, also c is -1 */
    if (point.r == -1)
        error ("Function get_first_box_ship (...), no 1 in matrix");

    new_c = (horizontal) ? point.c + blocks : point.c + 1;

    if (horizontal) {
        m_destination[point.r][point.c] = SEA;

        /* Out of range, we must change the row */
        if (new_c >= 10) {
            reset_row (m_destination, point.r);

            for (k = 0; k < blocks; k++)
                m_destination[point.r + 1][k] = SHIP;
        }
        /* Shift the bits to right */
        else
            for (k = new_c; k > point.c; k--)
                m_destination[point.r][k] = SHIP;
    }
    /* Vertical */
    else {
        reset_column (m_destination, point.c);

        /* Out of range, we must change the row */
        if (new_c >= 10)
            for (k = 0; k < blocks; k++)
                m_destination[point.r + k + 1][0] = SHIP;

        /* Shift column to right */
        else
            for (k = 0; k < blocks; k++)
                m_destination[point.r + k][new_c] = SHIP;
    }
}

void
get_correct_configurations (int m_1[][N][N], int l_1, int m_2[][N][N], int l_2,
                            int m_3[][N][N], int l_3, int m_4[][N][N], int l_4, option_ opt)
{
    clock_t start = clock (), end;

    int k, j, i, x, y, z, exit_loop, sum, n_matrixes = 0;

    /* Based on the number of matrixes, we can know the number of the blocks of the ship (2, 3, 4 or 5) */
    int n_blocks_m_1 = (l_1 == 180) ? 2 : (l_1 == 160) ? 3 : (l_1 == 140) ? 4 : 5,
        n_blocks_m_2 = (l_2 == 180) ? 2 : (l_2 == 160) ? 3 : (l_2 == 140) ? 4 : 5,
        n_blocks_m_3 = (l_3 == 180) ? 2 : (l_3 == 160) ? 3 : (l_3 == 140) ? 4 : 5,
        n_blocks_m_4 = (l_4 == 180) ? 2 : (l_4 == 160) ? 3 : (l_4 == 140) ? 4 : 5;

    coordinate* coordinates;

    int n_shots = 0;

    char file_name[100];

    FILE* fp;

    sprintf (file_name,
        opt == OPTION_CS ? PATH_SOLVING_PERMUTATIONS : PATH_CORRECT_CONFIGURATIONS,
        permutation_k
    );

    fp = fopen (file_name, "w");

    if (fp == NULL)
        error ("[-] fp is null");

    /* First iteration */
    for (k = 0; k < l_1; k++) {
        /* Second iteration */
        for (j = 0; j < l_2; j++) {
            /* Third iteration */
            for (i = 0; i < l_3; i++) {
                /* Fourth iteration */
                for (x = 0; x < l_4; x++) {
                    /* Combine matrixes */
                    exit_loop = 0;
                    for (y = 0; y < N; y++) {
                        for (z = 0; z < N; z++) {
                            sum = m_1[k][y][z] + m_2[j][y][z] + m_3[i][y][z] + m_4[x][y][z];

                            /* SEA = 1, SHIP = 2, so:
                             *      - if sum = 4 means that all elements [y][z] are set to SEA => no overlap;
                             *      - if sum = 5 means that all elements [y][z] are set to SEA except one element => no overlap;
                             *      - if sum > 5 means that more than one element[y][z] is set to SEA => overlap.
                             */
                            if (sum > 5) {
                                exit_loop = 1;

                                break;
                            }
                            if (sum == 4)
                                board[y][z] = SEA;
                            else if (sum == 5)
                                board[y][z] = SHIP;
                        }
                        if (exit_loop)
                            break;
                    }
                    /* Matrix could be ok, we must check the neighbours.
                     * We must respect the constraint:
                     * "Between the edges of each ship must be at least one box, even diagonally"
                     */
                    if (!exit_loop) {
                        /* First matrix */
                        coordinates = get_first_and_last_box_ship (m_1[k]);
                        if ((coordinates)->r == -1 || (coordinates + 1)->r == -1)
                            error ("No one in matrix");
                        if (!constraint_satisfied (board, coordinates, n_blocks_m_1))
                            continue;
                        free (coordinates);coordinates = NULL;

                        /* Second matrix */
                        coordinates = get_first_and_last_box_ship (m_2[j]);
                        if ((coordinates)->r == -1 || (coordinates + 1)->r == -1)
                            error ("No one in matrix");
                        if (!constraint_satisfied (board, coordinates, n_blocks_m_2))
                            continue;
                        free (coordinates);coordinates = NULL;

                        /* Third matrix */
                        coordinates = get_first_and_last_box_ship (m_3[i]);
                        if ((coordinates)->r == -1 || (coordinates + 1)->r == -1)
                            error ("No one in matrix");
                        if (!constraint_satisfied (board, coordinates, n_blocks_m_3))
                            continue;
                        free (coordinates);coordinates = NULL;

                        /* Fourth matrix */
                        coordinates = get_first_and_last_box_ship (m_4[x]);
                        if ((coordinates)->r == -1 || (coordinates + 1)->r == -1)
                            error ("No one in matrix");
                        if (!constraint_satisfied (board, coordinates, n_blocks_m_4))
                            continue;
                        free (coordinates);coordinates = NULL;

                        n_matrixes++;

                        if (opt == OPTION_CS) {
                            n_shots = solve ();

                            if (n_shots == -1) {
                                printf ("n_shots = -1\n");
                                print_matrix (board);

                                exit (-1);
                            }

                            fprintf (fp, "%d\n", n_shots);
                        }
                        else {
                            print_matrix_in_file (
                                fp,
                                l_1 == 120 ? m_1[k] : l_2 == 120 ? m_2[j] : l_3 == 120 ? m_3[i] : m_4[x],   /* Ship of 5 blocks */
                                l_1 == 140 ? m_1[k] : l_2 == 140 ? m_2[j] : l_3 == 140 ? m_3[i] : m_4[x],   /* Ship of 4 blocks */
                                l_1 == 160 ? m_1[k] : l_2 == 160 ? m_2[j] : l_3 == 160 ? m_3[i] : m_4[x],   /* Ship of 3 blocks */
                                l_1 == 180 ? m_1[k] : l_2 == 180 ? m_2[j] : l_3 == 180 ? m_3[i] : m_4[x]    /* Ship of 2 blocks */
                            );
                        }
                    } /* if !exitLoop */
                } /* End Fourth iteration */
            } /* End third iteration */
        } /* End second iteration */
    } /* End first iteration */

    fclose (fp);

    end = clock ();

    if (opt == OPTION_CS)
        printf ("[+] Number of shots for each correct configuration were printed inside \"%s\".\n", file_name);
    else
        printf ("[+] Correct configurations were printed inside \"%s\".\n", file_name);

    printf ("[+] Correct configurations: %d, time elapsed: %f\n\n", n_matrixes, (double) (end - start) / CLOCKS_PER_SEC);
}

int
constraint_satisfied (int m[N][N], coordinate* coordinates, int blocks)
{
    /*
     * Horizontal:
     *
     * x_3  x_1   y_1  y_3
     * x_4   1     1   y_4
     * x_5  x_2   y_2  y_5
     * 0 0 0 0
     *
     *                                          Ship of length 2
     *
     * Vertical:
     *
     * x_3  x_1  x_4  0
     * x_2   1   x_5  0
     * y_3   1   y_5  0
     * y_2  y_1  y_4  0
     * 0     0    0   0
     */


    /*
     * Horizontal:
     *
     * x_3  x_1   Z'    y_1  y_3
     * x_4   1    1      1   y_4
     * x_5  x_2   Z''   y_2  y_5
     * 0 0 0 0
     *
     *                                          Ship of length 3
     *
     * Vertical:
     *
     * x_3  x_1  x_4  0
     * x_2   1   x_5  0
     * Z'    1    Z'' 0
     * y_3   1   y_5  0
     * y_2  y_1  y_4  0
     * 0     0    0   0
     */


    /*
     * Horizontal:
     *
     * x_3  x_1   Z'   Z'''  y_1  y_3
     * x_4   1    1    1      1   y_4
     * x_5  x_2   Z''  Z'''' y_2  y_5
     * 0 0 0 0
     *
     *                                          Ship of length 4
     *
     * Vertical:
     *
     * x_3  x_1  x_4   0
     * x_2   1   x_5   0
     * Z'    1    Z''  0
     * Z'''  1   Z'''' 0
     * y_3   1   y_5   0
     * y_2  y_1  y_4   0
     * 0     0    0    0
     */


    /*
     * Horizontal:
     *
     * x_3  x_1   Z'   Z'''   Z'''''     y_1  y_3
     * x_4   1    1    1      1           1   y_4
     * x_5  x_2   Z''  Z''''  Z''''''    y_2  y_5
     * 0 0 0 0
     *
     *                                          Ship of length 5
     *
     * Vertical:
     *
     * x_3     x_1  x_4     0
     * x_2      1   x_5     0
     * Z'       1    Z''    0
     * Z'''     1   Z''''   0
     * Z'''''   1   Z'''''' 0
     * y_3      1   y_5     0
     * y_2     y_1  y_4     0
     * 0        0    0      0
     */
    int r, c;

    if ((coordinates)->r == (coordinates + 1)->r) {
        /*
         * Horizontal:
         *
         * x_3  x_1   y_1  y_3
         * x_4   1     1   y_4
         * x_5  x_2   y_2  y_5
         * 0 0 0 0
         */
        r = (coordinates)->r;
        c = (coordinates)->c;

        /* Check x_k */
        if (r - 1 >= 0 && m[r - 1][c] == SHIP) return 0; /* x_1 */
        if (r + 1 <= 9 && m[r + 1][c] == SHIP) return 0; /* x_2 */
        if (r - 1 >= 0 && c - 1 >= 0 && m[r - 1][c - 1] == SHIP) return 0; /* x_3 */
        if (c - 1 >= 0 && m[r][c - 1] == SHIP) return 0; /* x_4 */
        if (r + 1 <= 9 && c - 1 >= 0 && m[r + 1][c - 1] == SHIP) return 0; /* x_5 */

        /* Check y_k */
        r = (coordinates + 1)->r;
        c = (coordinates + 1)->c;

        if (r - 1 >= 0 && m[r - 1][c] == SHIP) return 0; /* y_1 */
        if (r + 1 <= 9 && m[r + 1][c] == SHIP) return 0; /* y_2 */
        if (r - 1 >= 0 && c + 1 <= 9 && m[r - 1][c + 1] == SHIP) return 0; /* y_3 */
        if (c + 1 <= 9 && m[r][c + 1] == SHIP) return 0; /* y_4 */
        if (r + 1 <= 9 && c + 1 <= 9 && m[r + 1][c + 1] == SHIP) return 0; /* y_5 */

        /* Iterate with "for", would be more readable. Anyway we use this version */
        if (blocks >= 3) {
            /* Restart with the first box to get the second, third and so on */
            r = (coordinates)->r;
            c = (coordinates)->c + 1;

            if (c <= 9) {
                if (r - 1 >= 0 && m[r - 1][c] == SHIP) return 0; /* Z' */
                if (r + 1 <= 9 && m[r + 1][c] == SHIP) return 0; /* Z'' */
            }

            if (blocks >= 4) {
                c++;

                if (c <= 9) {
                    if (r - 1 >= 0 && m[r - 1][c] == SHIP) return 0; /* Z''' */
                    if (r + 1 <= 9 && m[r + 1][c] == SHIP) return 0; /* Z'''' */
                }

                if (blocks == 5) {
                    c++;

                    if (c <= 9) {
                        if (r - 1 >= 0 && m[r - 1][c] == SHIP) return 0; /* Z''''' */
                        if (r + 1 <= 9 && m[r + 1][c] == SHIP) return 0; /* Z'''''' */
                    }
                }
            }
        }
    }
    else {
        /*
         * Vertical:
         *
         * x_3  x_1  x_4  0
         * x_2   1   x_5  0
         * y_3   1   y_5  0
         * y_2  y_1  y_4  0
         * 0     0    0   0
         */
        r = (coordinates)->r;
        c = (coordinates)->c;

        /* Check x_k */
        if (r - 1 >= 0 && m[r - 1][c] == SHIP) return 0; /* x_1 */
        if (c - 1 >= 0 && m[r][c - 1] == SHIP) return 0; /* x_2 */
        if (r - 1 >= 0 && c - 1 >= 0 && m[r - 1][c - 1] == SHIP) return 0; /* x_3 */
        if (r - 1 >= 0 && c + 1 <= 9 && m[r - 1][c + 1] == SHIP) return 0; /* x_4 */
        if (c + 1 <= 9 && m[r][c + 1] == SHIP) return 0; /* x_5 */

        /* Check y_k */
        r = (coordinates + 1)->r;
        c = (coordinates + 1)->c;

        if (r + 1 <= 9 && m[r + 1][c] == SHIP) return 0; /* y_1 */
        if (r + 1 <= 9 && c - 1 >= 0 && m[r + 1][c - 1] == SHIP) return 0; /* y_2 */
        if (c - 1 >= 0 && m[r][c - 1] == SHIP) return 0; /* y_3 */
        if (r + 1 <= 9 && c + 1 <= 9 && m[r + 1][c + 1] == SHIP) return 0; /* y_4 */
        if (c + 1 <= 9 && m[r][c + 1] == SHIP) return 0; /* y_5 */

        /* Iterate with "for", would be more readable. Anyway we use this version */
        if (blocks >= 3) {
            /* Restart with the first box to get the second, third and so on */
            r = (coordinates)->r + 1;
            c = (coordinates)->c;

            if (r <= 9) {
                if (c - 1 >= 0 && m[r][c - 1] == SHIP) return 0; /* Z' */
                if (c + 1 <= 9 && m[r][c + 1] == SHIP) return 0; /* Z'' */
            }

            if (blocks >= 4) {
                r++;

                if (r <= 9) {
                    if (c - 1 >= 0 && m[r][c - 1] == SHIP) return 0; /* Z''' */
                    if (c + 1 <= 9 && m[r][c + 1] == SHIP) return 0; /* Z'''' */
                }

                if (blocks == 5) {
                    r++;

                    if (r <= 9) {
                        if (c - 1 >= 0 && m[r][c - 1] == SHIP) return 0; /* Z''''' */
                        if (c + 1 <= 9 && m[r][c + 1] == SHIP) return 0; /* Z'''''' */
                    }
                }
            }
        }
    }

    return 1;
}

coordinate
get_first_box_ship (int m[N][N])
{
    int k, j;
    coordinate c;

    c.r = -1;
    c.c = -1;

    for (k = 0; k < N; k++) {
        for (j = 0; j < N; j++) {
            if (m[k][j] == SHIP) {
                c.r = k;
                c.c = j;

                return c;
            }
        }
    }

    return c;
}

coordinate
get_last_box_ship (int m[N][N])
{
    int k, j;
    coordinate c;

    c.r = -1;
    c.c = -1;

    for (k = (N - 1); k >= 0; k--) {
        for (j = (N - 1); j >= 0; j--) {
            if (m[k][j] == SHIP) {
                c.r = k;
                c.c = j;

                return c;
            }
        }
    }

    return c;
}

coordinate*
get_first_and_last_box_ship (int m[N][N])
{
    int k;

    coordinate first = get_first_box_ship (m),
               last  = get_last_box_ship (m) ;

    coordinate* first_and_last = (coordinate *) malloc (2 * sizeof (coordinate));

    /* First */
    (first_and_last)->r = first.r;
    (first_and_last)->c = first.c;

    (first_and_last + 1)->r = last.r;
    (first_and_last + 1)->c = last.c;

    /* Once we get the first 1, the last will be on the right or down */
    /* Try right */
    if (first.c + 1 < N && m[first.r][first.c + 1] == SHIP) {
        for (k = first.c + 1; k < N; k++) {
            if (m[first.r][k] == SEA) {
                (first_and_last + 1)->r = first.r;
                (first_and_last + 1)->c = k - 1;

                break;
            }
        }
    }
    /* Last 1 is down */
    else if (first.r + 1 < N && m[first.r + 1][first.c] == SHIP) {
        for (k = first.r + 1; k < N; k++) {
            if (m[k][first.c] == SEA) {
                (first_and_last + 1)->r = k - 1;
                (first_and_last + 1)->c = first.c;

                break;
            }
        }
    }
    /* Something goes wrong */
    else {
        (first_and_last + 1)->r = -1;
        (first_and_last + 1)->c = -1;
    }

    return first_and_last;
}

void
reset_row (int m[N][N], int row)
{
    int k;

    assert__ (row < N, "reset_row (...) row >= N");

    for (k = 0; k < N; k++)
        m[row][k] = SEA;
}

void
reset_column (int m[N][N], int column)
{
    int k;

    assert__ (column < N, "reset_column (...) column >= N");

    for (k = 0; k < N; k++)
        m[k][column] = SEA;
}

void
copy_values_matrixes (int m_source[N][N], int m_destination[N][N])
{
    int k, j;

    for (k = 0; k < N; k++)
        for (j = 0; j < N; j++)
            m_destination[k][j] = m_source[k][j];
}

void
print_matrix (int m[N][N])
{
    int k, j;

    for (k = 0; k < N; k++) {
        printf ("\n");

        for (j = 0; j < N; j++)
            printf ("%3d ", m[k][j]);
    }

    printf ("\n");
}

void
print_matrix_in_file (FILE* fp, int m_5[N][N], int m_4[N][N], int m_3[N][N], int m_2[N][N])
{
    echo_grid_notation (fp, m_5, 5);
    echo_grid_notation (fp, m_4, 4);
    echo_grid_notation (fp, m_3, 3);
    echo_grid_notation (fp, m_2, 2);
}

void
echo_grid_notation (FILE* fp, int m[N][N], int blocks)
{
    int k, j;

    char buf[80];

    int size = 0, counter = 0;

    for (k = 0; k < N; k++) {
        for (j = 0; j < N; j++) {
            if (m[k][j] == SHIP) {
                /* Get column */
                buf[size++] = j + 'A';

                /* Get row. Grid starts with 1 but matrix in 0 */
                buf[size++] = (k == 9) ? '1' : (k + '1');

                if (k == 9)
                    buf[size++] = '0';

                counter++;

                if (counter == blocks) {
                    buf[size++] = '\n';
                    buf[size++] = '\0';

                    fprintf (fp, "%s", buf);

                    return;
                }
            }
        }
    }
}

int
file_exists (char* file)
{
    FILE* fp = fopen (file, "r");
    int ret  = (fp == NULL) ? 0 : 1;

    fclose (fp);

    return ret;
}

int
is_number (char str[])
{
    int k, len;

    for (k = 0, len = strlen (str); k < len; k++)
        if (str[k] < '0' || str[k] > '9')
            return 0;

    return 1;
}

void
assert__ (int condition, char* message)
{
    if (!condition)
        error (message);
}
