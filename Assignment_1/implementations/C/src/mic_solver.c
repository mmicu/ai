#include "mic_solver.h"

/* "board" is the global matrix declared inside "main.h" */

int
solve (void)
{
    int k;

    coordinate point;

    coordinate* points_red;
    coordinate* points_blue;
    coordinate* points_green;

    /* Global variables "number_shots" and "remained_ships" */
    number_shots = 0;

    remained_ships[0] = 2;
    remained_ships[1] = 3;
    remained_ships[2] = 4;
    remained_ships[3] = 5;

    /* Main diagonal (D1) */
    for (k = 0; k < N; k++) {
        point.r = k;
        point.c = k;

        try_to_shot (point);

        if (finish ())
            return number_shots;
    }

    /* Second diagonal (D2) */
    for (k = 0; k < N; k++) {
        point.r = k;
        point.c = N - 1 - k;

        try_to_shot (point);

        if (finish ())
            return number_shots;
    }

    /* D3 */
    for (k = 0; k < 5; k++) {
        point.r = k;
        point.c = N - 6 - k;

        /* Jump (2, 2), already check in D1 */
        if (k == 2)
            continue;

        try_to_shot (point);

        if (finish ())
            return number_shots;
    }
    /* With D1 and D3 I isolate 2 zones => B1 (RED block, ships of length 2 or 3) */
    points_red = (coordinate *) malloc (sizeof (coordinate));
    /* (RED) First zone of B1 */
    points_red->r = 0;
    points_red->c = 2;
    shot_coloured_zone (RED, points_red);
    if (finish ()) {
        free (points_red);

        return number_shots;
    }
    /* (RED) Second zone of B1 */
    points_red->r = 2;
    points_red->c = 0;
    shot_coloured_zone (RED, points_red);
    if (finish ()) {
        free (points_red);

        return number_shots;
    }


    /* D4 */
    for (k = 0; k < 5; k++) {
        point.r = k;
        point.c = N - 5 + k;

        /* Jump (2, 7), already check in D2 */
        if (k == 2)
            continue;

        try_to_shot (point);
        if (finish ()) {
            free (points_red);

            return number_shots;
        }
    }
    /* With D2 and D4 I isolate 2 zones => B2 (RED block, ships of length 2 or 3) */
    /* (RED) First zone of B2 */
    points_red->r = 0;
    points_red->c = 7;
    shot_coloured_zone (RED, points_red);
    if (finish ()) {
        free (points_red);

        return number_shots;
    }
    /* (RED) Second zone of B2 */
    points_red->r = 2;
    points_red->c = 9;
    shot_coloured_zone (RED, points_red);
    if (finish ()) {
        free (points_red);

        return number_shots;
    }

    /* With D1, D2, D3 and D4 I isolate 1 zone => B6 (GREEN zone, ships of length 2, 3 and 4) */
    points_green = (coordinate *) malloc (4 * sizeof (coordinate));
    points_green->r       = 1; points_green->c       = 4;
    (points_green + 1)->r = 2; (points_green + 1)->c = 4;
    (points_green + 2)->r = 3; (points_green + 2)->c = 4;
    (points_green + 3)->r = 2; (points_green + 3)->c = 5;
    shot_coloured_zone (GREEN, points_green);
    if (finish ()) {
        free (points_red);
        free (points_green);

        return number_shots;
    }


    /* D5 */
    for (k = 0; k < 5; k++) {
        point.r = k + 5;
        point.c = k;

        /* Jump (7, 2), already check in D2 */
        if (k == 2)
            continue;

        try_to_shot (point);
        if (finish ()) {
            free (points_red);
            free (points_green);

            return number_shots;
        }
    }
    /* With D2 and D5 I isolate 2 zones => B3 (RED block, ships of length 2 and 3) */
    /* (RED) First zone of B3 */
    points_red->r = 7;
    points_red->c = 0;
    shot_coloured_zone (RED, points_red);
    if (finish ()) {
        free (points_red);
        free (points_green);

        return number_shots;
    }

    /* (RED) Second zone of B3 */
    points_red->r = 9;
    points_red->c = 2;
    shot_coloured_zone (RED, points_red);
    if (finish ()) {
        free (points_red);
        free (points_green);

        return number_shots;
    }

    /* With D1, D2, D3 and D5 I isolate 1 zone => B4 (BLUE zone, ships of length 2, 3 and 4) */
    points_blue = (coordinate *) malloc (4 * sizeof (coordinate));
    points_blue->r       = 5; points_blue->c       = 1;
    (points_blue + 1)->r = 4; (points_blue + 1)->c = 2;
    (points_blue + 2)->r = 5; (points_blue + 2)->c = 2;
    (points_blue + 3)->r = 5; (points_blue + 3)->c = 3;
    shot_coloured_zone (BLUE, points_blue);
    if (finish ()) {
        free (points_red);
        free (points_green);
        free (points_blue);

        return number_shots;
    }


    /* D6 */
    for (k = 0; k < 5; k++) {
        point.r = k + 5;
        point.c = N - 1 - k;

        /* Jump (7, 7), already check in D1 */
        if (k == 2)
            continue;

        try_to_shot (point);

        if (finish ()) {
            free (points_red);
            free (points_green);
            free (points_blue);

            return number_shots;
        }
    }
    /* With D1 and D6 I isolate 2 zones => B4 (RED block, ships of length 2 and 3) */
    /* (RED) First zone of B4 */
    points_red->r = 9;
    points_red->c = 7;
    shot_coloured_zone (RED, points_red);
    if (finish ()) {
        free (points_red);
        free (points_green);
        free (points_blue);

        return number_shots;
    }

    /* (RED) Second zone of B4 */
    points_red->r = 7;
    points_red->c = 9;
    shot_coloured_zone (RED, points_red);
    if (finish ()) {
        free (points_red);
        free (points_green);
        free (points_blue);

        return number_shots;
    }

    /* With D1, D2, D4 and D6 I isolate 1 zone => B5 (BLUE zone, ships of length 2, 3 and 4) */
    points_blue->r       = 5; points_blue->c       = 6;
    (points_blue + 1)->r = 4; (points_blue + 1)->c = 7;
    (points_blue + 2)->r = 5; (points_blue + 2)->c = 7;
    (points_blue + 3)->r = 5; (points_blue + 3)->c = 8;
    shot_coloured_zone (BLUE, points_blue);
    if (finish ()) {
        free (points_red);
        free (points_green);
        free (points_blue);

        return number_shots;
    }

    /* With D1, D2, D5 and D6 I isolate 1 zone => B7 (GREEN zone, ships of length 2, 3 and 4) */
    points_green->r       = 6; points_green->c       = 4;
    (points_green + 1)->r = 7; (points_green + 1)->c = 4;
    (points_green + 2)->r = 8; (points_green + 2)->c = 4;
    (points_green + 3)->r = 7; (points_green + 3)->c = 5;
    shot_coloured_zone (GREEN, points_green);

    free (points_red);
    free (points_green);
    free (points_blue);

    return finish () ? number_shots : -1;
}

int
try_to_shot (coordinate point)
{
    int ret = 0;

    /* We hit part of the ship */
    if (shot (point))
        /* First, we try to shot horizontally and if we hadn't hit part of the ship, we try vertically.
         * If the ship is vertically oriented, function returns 1 else 0.
         */
        ret = (shot_ship (point, 0)) ? 1 : shot_ship (point, 1);

    return ret;
}

int
shot (coordinate point)
{
    /* There is the possibility that, for some errors, program will call this function two or more times, with the same value
     * for the element "r" and "c" of point. So, incrementation of "number_shots" will be done if and only if the value
     * of the box for the point passed as value, is SEA or SHIP
     */
    if (board[point.r][point.c] == SEA || board[point.r][point.c] == SHIP) {
        number_shots++;

        board[point.r][point.c] = (board[point.r][point.c] == SEA) ? SHOT_FREE : SHOT_BOX_SHIP;

        return board[point.r][point.c] == SHOT_BOX_SHIP;
    }

    return 0;
}

int
shot_ship (coordinate point_hit, int is_vertical)
{
    /* r, c are the coordinates of one box of the ship that we had hit.
     * But we don't know if it's the first box, the second and so on.
     * We only know the direction of the ship and the coordinates of the box hit
     */

    /* How many blocks have the ship? */
    int blocks = 1;   /* Starts with 1 since we have already hit a box */

    int r_app, c_app;

    /* To know how we can eliminate the neighbors of the ship */
    coordinate start, end, new_point;

    /* Init start */
    start.r = point_hit.r;
    start.c = point_hit.c;
    /* Init end */
    end.r = point_hit.r;
    end.c = point_hit.c;

    /* Vertical direction */
    if (is_vertical) {
        r_app = point_hit.r - 1;

        /* Try to go up */
        while (r_app >= 0) {
            new_point.r = r_app;
            new_point.c = point_hit.c;

            /* We hit a box */
            if (shot (new_point)) {
                blocks++;

                start.r = r_app;
                start.c = point_hit.c;
            }
            else
                break;

            r_app--;
        }

        /*
         * At this point there is two possibilities:
         *  1) I've hit a box on the "while" up, in this case there are also two possibilities:
         *      1.1) Box or boxes that I hit up, can be complete the ship;
         *      1.2) Box or boxes that I hit up, is/are not enough to hit all the ship.
         *  2) I've not hit a box on the "while" up (hit_up = false) => ship are surely on the buttom
         */

        /* 1.1) Condition */
        /*if (n_remained_ships () == 1 && contains_ship (blocks)) {
            remove_ship (blocks);
            printf ("CIAO 222\n");
            return 1;
        }*/

        /* Go here means that we don't have enough informations to say that the ship is completed hit */
        /* Go down */
        r_app  = point_hit.r + 1;

        while (r_app <= 9) {
            new_point.r = r_app;
            new_point.c = point_hit.c;

            /* We hit a box */
            if (shot (new_point)) {
                blocks++;

                end.r = r_app;
                end.c = point_hit.c;
            }
            else
                break;

            r_app++;
        }
    }
    /* Horizontal direction */
    else {
        c_app = point_hit.c - 1;

        /* Try to go left */
        while (c_app >= 0) {
            new_point.r = point_hit.r;
            new_point.c = c_app;

            /* We hit a box */
            if (shot (new_point)) {
                blocks++;

                start.r = point_hit.r;
                start.c = c_app;
            }
            else
                break;

            c_app--;
        }

        /*
         * At this point there is two possibilities:
         *  1) I've hit a box on the while up, in this case there are also two possibilities:
         *      1.1) Box or boxes that I hit up, can be complete the ship;
         *      1.2) Box or boxes that I hit up, is/are not enough to hit all the ship;
         *  2) I've not hit a box on the while up (hit_up = false) => ship are surely on the buttom.
         */

        /* 1.1) Condition */
        /*if (n_remained_ships () == 1 && contains_ship (blocks)) {
            remove_ship (blocks);

            printf ("CIAO %d\n",blocks);

            return 1;
        }*/

        /* Go here means that we don't have enough informations to say that the ship is completed hit */
        /* Go to right */
        c_app = point_hit.c + 1;

        while (c_app <= 9) {
            new_point.r = point_hit.r;
            new_point.c = c_app;

            /* We hit a box */
            if (shot (new_point)) {
                blocks++;

                end.r = point_hit.r;
                end.c = c_app;
            }
            else
                break;

            c_app++;
        }
    }

    /* Remove the ships */

    if (blocks > 1) {
        remove_ship (blocks);

        eliminate_neighbors_ship (start, end, blocks);
    }

    return (blocks == 1) ? 0 : 1;
}

int
shot_coloured_zone (int color, coordinate* points)
{
    coordinate p;
    int size_points = (color == RED) ? 1 : 4;
    int r, c, k;

    switch (color) {
    case RED:
        /* I can exclude all configurations, since ships with length 2 and 3 they were sunk */
        if (!contains_ship (2) && !contains_ship (3)) {
            r = (points)->r;
            c = (points)->c;

            if (board[r][c] == SEA)
                board[r][c] = EXCLUDED;

            /* Up */
            if (r - 1 >= 0 && board[r - 1][c] == SEA)
                board[r - 1][c] = EXCLUDED;

            /* Down */
            if (r + 1 < N && board[r + 1][c] == SEA)
                board[r + 1][c] = EXCLUDED;

            /* Left */
            if (c - 1 >= 0 && board[r][c - 1] == SEA)
                board[r][c - 1] = EXCLUDED;

            /* Right */
            if (c + 1 < N && board[r][c + 1] == SEA)
                board[r][c + 1] = EXCLUDED;
        }
        else {
            /* Central point */
            p.r = (points)->r;
            p.c = (points)->c;

            try_to_shot (p);
        }
        break;
    case BLUE:
        /* I can exclude all configurations, since ships with length 2, 3 and 4 they were sunk */
        if (!contains_ship (2) && !contains_ship (3) && !contains_ship (4)) {
            /* We have four points for each of the 2 BLUE zone:
             *      - first BLUE:   (5, 1), (4, 2), (5, 2), (5, 3);
             *      - second BLUE:  (5, 6), (4, 7), (5, 7), (5, 8).
             */
            r = (points)->r;
            c = (points)->c;

            /* If c = 1, we are in the first BLUE zone (B4) else in the second BLUE zone (B5) */
            if (c == 1 || c == 6) {
                if (board[4][c == 1 ? 1 : 6] == SEA)
                    board[4][c == 1 ? 1 : 6] = EXCLUDED;

                if (board[5][c == 1 ? 1 : 6] == SEA)
                    board[5][c == 1 ? 1 : 6] = EXCLUDED;

                if (board[3][c == 1 ? 2 : 7] == SEA)
                    board[3][c == 1 ? 2 : 7] = EXCLUDED;

                if (board[4][c == 1 ? 2 : 7] == SEA)
                    board[4][c == 1 ? 2 : 7] = EXCLUDED;

                if (board[5][c == 1 ? 2 : 7] == SEA)
                    board[5][c == 1 ? 2 : 7] = EXCLUDED;

                if (board[6][c == 1 ? 2 : 7] == SEA)
                    board[6][c == 1 ? 2 : 7] = EXCLUDED;

                if (board[4][c == 1 ? 3 : 8] == SEA)
                    board[4][c == 1 ? 3 : 8] = EXCLUDED;

                if (board[5][c == 1 ? 3 : 8] == SEA)
                    board[5][c == 1 ? 3 : 8] = EXCLUDED;
            }
            else
                error ("shot_coloured_zone (...), BLUE zone, c is different from 1 and 6");
        }
        else {
            /* In the BLUE zone, there is at most 1 ship of length 2, 3 or 4 */
            for (k = 0; k < size_points; k++) {
                p.r = (points + k)->r;
                p.c = (points + k)->c;

                if (contains_ship (2)) {
                    if (try_to_shot (p))
                        return 1;
                }
                else if (contains_ship (3)) {
                    /* Is enough to shot on second AND third point */
                    if (k == 1 || k == 2)
                        if (try_to_shot (p))
                            return 1;
                }
                /* Only possibility to ship of 4, is vertical position, there is not horizontal space */
                else if (contains_ship (4)) {
                    /* Is enough to shot on second OR third point => shot on the second */
                    if (k == 1)
                        return try_to_shot (p);
                }
            }
        }
        break;
    case GREEN:
        /* I can exclude all configurations, since ships with length 2, 3 and 4 they were sunk */
        if (!contains_ship (2) && !contains_ship (3) && !contains_ship (4)) {
            /* We have four points for each of the 2 BLUE zone:
             *      - first GREEN:    (1, 4), (2, 4), (3, 4), (2, 5).
             *      - second GREEN:   (6, 4), (7, 4), (8, 4), (7, 5);
             */
            r = (points)->r;
            c = (points)->c;

            /* If r = 6, we are in the first GREEN zone (B6) else in the second GREEN zone (B7) */
            if (r == 1 || r == 6) {
                if (board[r == 1 ? 2 : 7][3] == SEA)
                    board[r == 1 ? 2 : 7][3] = EXCLUDED;

                if (board[r == 1 ? 1 : 6][4] == SEA)
                    board[r == 1 ? 1 : 6][4] = EXCLUDED;

                if (board[r == 1 ? 2 : 7][4] == SEA)
                    board[r == 1 ? 2 : 7][4] = EXCLUDED;

                if (board[r == 1 ? 3 : 8][4] == SEA)
                    board[r == 1 ? 3 : 8][4] = EXCLUDED;

                if (board[r == 1 ? 1 : 6][5] == SEA)
                    board[r == 1 ? 1 : 6][5] = EXCLUDED;

                if (board[r == 1 ? 2 : 7][5] == SEA)
                    board[r == 1 ? 2 : 7][5] = EXCLUDED;

                if (board[r == 1 ? 3 : 8][5] == SEA)
                    board[r == 1 ? 3 : 8][5] = EXCLUDED;

                if (board[r == 1 ? 2 : 7][6] == SEA)
                    board[r == 1 ? 2 : 7][6] = EXCLUDED;
            }
            else
                error ("shot_coloured_zone (...), BLUE zone, c is different from 1 and 6");
        }
        else {
            /* In the GREEN zone, there is at most 1 ship of length 2, 3 or 4 */
            for (k = 0; k < size_points; k++) {
                p.r = (points + k)->r;
                p.c = (points + k)->c;

                if (contains_ship (2)) {
                    if (try_to_shot (p))
                        return 1;
                }
                else if (contains_ship (3)) {
                    /* Is enough to shot on second AND fourth point */
                    if (k == 1 || k == 3)
                        if (try_to_shot (p))
                            return 1;
                }
                /* Only possibility to ship of 4, is vertical position, there is not horizontal space */
                else if (contains_ship (4)) {
                    /* Is enough to shot on second OR fourth point => shot on the second */
                    if (k == 1)
                        return try_to_shot (p);
                }
            }
        }
        break;
    }

    return 0;
}

coordinate*
get_horizontal_neighbors (coordinate point)
{
    int size = (point.c - 1 >= 0 && point.c + 1 < N) ? 2 : 1;

    coordinate* horizontal_neighbors = (coordinate *) malloc (size * sizeof (coordinate));

    if (point.c - 1 >= 0) {
        (horizontal_neighbors)->r = point.r;
        (horizontal_neighbors)->c = point.c - 1;
    }

    if (point.c + 1 < N) {
        (horizontal_neighbors + (size - 1))->r = point.r;
        (horizontal_neighbors + (size - 1))->c = point.c + 1;
    }

    return horizontal_neighbors;
}

coordinate*
get_vertical_neighbors (coordinate point)
{
    int size = (point.r - 1 >= 0 && point.r + 1 < N) ? 2 : 1;

    coordinate* vertical_neighbors = (coordinate *) malloc (size * sizeof (coordinate));

    if (point.r - 1 >= 0) {
        (vertical_neighbors)->r = point.r - 1;
        (vertical_neighbors)->c = point.c;
    }

    if (point.r + 1 < N) {
        (vertical_neighbors + (size - 1))->r = point.r + 1;
        (vertical_neighbors + (size - 1))->c = point.c;
    }

    return vertical_neighbors;
}

void
eliminate_neighbors_ship (coordinate start, coordinate end, int blocks)
{
    int r, c;
    /*
     * * Horizontal:
     *
     * x_3  x_1   Z'   Z'''   Z'''''     y_1  y_3
     * x_4   1    1    1      1           1   y_4
     * x_5  x_2   Z''  Z''''  Z''''''    y_2  y_5
     * 0 0 0 0
     */
    /* Horizontal orientation */
    if (start.r == end.r) {
        r = start.r;
        c = start.c;

        if (r - 1 >= 0 && board[r - 1][c] == SEA)                   board[r - 1][c]     = EXCLUDED;
        if (r + 1 <= 9 && board[r + 1][c] == SEA)                   board[r + 1][c]     = EXCLUDED;
        if (r - 1 >= 0 && c - 1 >= 0 && board[r - 1][c - 1] == SEA) board[r - 1][c - 1] = EXCLUDED;
        if (c - 1 >= 0 && board[r][c - 1] == SEA)                   board[r][c - 1]     = EXCLUDED;
        if (r + 1 <= 9 && c - 1 >= 0 && board[r + 1][c - 1] == SEA) board[r + 1][c - 1] = EXCLUDED;

        r = end.r;
        c = end.c;

        if (r - 1 >= 0 && board[r - 1][c] == SEA)                   board[r - 1][c]     = EXCLUDED;
        if (r + 1 <= 9 && board[r + 1][c] == SEA)                   board[r + 1][c]     = EXCLUDED;
        if (r - 1 >= 0 && c + 1 <= 9 && board[r - 1][c + 1] == SEA) board[r - 1][c + 1] = EXCLUDED;
        if (c + 1 <= 9 && board[r][c + 1] == SEA)                   board[r][c + 1]     = EXCLUDED;
        if (r + 1 <= 9 && c + 1 <= 9 && board[r + 1][c + 1] == SEA) board[r + 1][c + 1] = EXCLUDED;

        if (blocks >= 3) {
            /* r remains the same, c must be incremented by 1 */
            r = start.r;
            c = start.c + 1;

            if (c <= 9) {
                if (r - 1 >= 0 && board[r - 1][c] == SEA) board[r - 1][c] = EXCLUDED;
                if (r + 1 <= 9 && board[r + 1][c] == SEA) board[r + 1][c] = EXCLUDED;
            }

            if (blocks >= 4) {
                /* r remains the same, c must be incremented by 1 */
                c++;

                if (c <= 9) {
                    if (r - 1 >= 0 && board[r - 1][c] == SEA) board[r - 1][c] = EXCLUDED;
                    if (r + 1 <= 9 && board[r + 1][c] == SEA) board[r + 1][c] = EXCLUDED;
                }

                if (blocks == 5) {
                    /* r remains the same, c must be incremented by 1 */
                    c++;

                    if (c <= 9) {
                        if (r - 1 >= 0 && board[r - 1][c] == SEA) board[r - 1][c] = EXCLUDED;
                        if (r + 1 <= 9 && board[r + 1][c] == SEA) board[r + 1][c] = EXCLUDED;
                    }
                } /* if (blocks >= 5) */
            } /* if (blocks >== SEA) */
        } /* if (blocks >= 3) */
    }
    /* Vertical orientation */
    else {
        r = start.r;
        c = start.c;

        /*  Check x_k */
        if (r - 1 >= 0 && board[r - 1][c] == SEA)                   board[r - 1][c]     = EXCLUDED;
        if (c - 1 >= 0 && board[r][c - 1] == SEA)                   board[r][c - 1]     = EXCLUDED;
        if (r - 1 >= 0 && c - 1 >= 0 && board[r - 1][c - 1] == SEA) board[r - 1][c - 1] = EXCLUDED;
        if (r - 1 >= 0 && c + 1 <= 9 && board[r - 1][c + 1] == SEA) board[r - 1][c + 1] = EXCLUDED;
        if (c + 1 <= 9 && board[r][c + 1] == SEA)                   board[r][c + 1]     = EXCLUDED;

        /* Check y_k */
        r = end.r;
        c = end.c;

        if (r + 1 <= 9 && board[r + 1][c] == SEA)                   board[r + 1][c]     = EXCLUDED;
        if (r + 1 <= 9 && c - 1 >= 0 && board[r + 1][c - 1] == SEA) board[r + 1][c - 1] = EXCLUDED;
        if (c - 1 >= 0 && board[r][c - 1] == SEA)                   board[r][c - 1]     = EXCLUDED;
        if (r + 1 <= 9 && c + 1 <= 9 && board[r + 1][c + 1] == SEA) board[r + 1][c + 1] = EXCLUDED;
        if (c + 1 <= 9 && board[r][c + 1] == SEA)                   board[r][c + 1]     = EXCLUDED;

        if (blocks >= 3) {
            /* c remains the same, r must be incremented by 1 */
            r = start.r + 1;
            c = start.c;

            if (r <= 9) {
                if (c - 1 >= 0 && board[r][c - 1] == SEA) board[r][c - 1] = EXCLUDED;
                if (c + 1 <= 9 && board[r][c + 1] == SEA) board[r][c + 1] = EXCLUDED;
            }

            if (blocks >= 4) {
                /* c remains the same, r must be incremented by 1 */
                r++;

                if (r <= 9) {
                    if (c - 1 >= 0 && board[r][c - 1] == SEA) board[r][c - 1] = EXCLUDED;
                    if (c + 1 <= 9 && board[r][c + 1] == SEA) board[r][c + 1] = EXCLUDED;
                }

                if (blocks == 5) {
                    /* c remains the same, r must be incremented by 1 */
                    r++;

                    if (r <= 9) {
                        if (c - 1 >= 0 && board[r][c - 1] == SEA) board[r][c - 1] = EXCLUDED;
                        if (c + 1 <= 9 && board[r][c + 1] == SEA) board[r][c + 1] = EXCLUDED;
                    }
                } /* if (blocks >= 5) */
            } /* if (blocks >= 4) */
        } /* if (blocks >= 3) */
    }
}

int
n_remained_ships (void)
{
    return contains_ship (2) + contains_ship (3) + contains_ship (4) + contains_ship (5);
}

void
remove_ship (int blocks)
{
    int k = 0;

    for (k = 0; k < N_SHIPS; k++)
        if (remained_ships[k] == blocks)
            remained_ships[k] = -1;
}

int
contains_ship (int blocks)
{
    int k = 0;

    for (k = 0; k < N_SHIPS; k++)
        if (remained_ships[k] == blocks)
            return 1;

    return 0;
}

int
count_occurences (int value)
{
    int k, j, counter = 0;

    for (k = 0; k < N; k++)
        for (j = 0; j < N; j++)
            if (board[k][j] == value)
                counter++;

    return counter;
}

int
finish (void)
{
    int ret = !contains_ship (2) && !contains_ship (3) && !contains_ship (4) && !contains_ship (5);

    if (ret) {
        assert__ (count_occurences (SHOT_BOX_SHIP) == 14, "ret is 1 but there is not 14 occurences of -2 in matrix");
        assert__ (number_shots >= 14, "ret is 1 but number_shots < 14");
    }

    return ret;
}
